variable "resource_group_name" {
  description = "Name of the resource group in which the resources will be created"
  default     = "SHIResourceGroup"
}

variable "location" {
  default     = "centralus"
  description = "Location where resources will be created"
}

variable "domain_name" {
  default     = "shiinterview"
}

variable "tags" {
  description = "Map of the tags to use for the resources that are deployed"
  type        = map(string)
  default = {
    environment = "webserver"
  }
}

variable "application_port" {
  description = "Port that you want to expose to the external load balancer"
  default     = 80
}

variable "admin_user" {
  description = "User name to use as the admin account on the VMs that will be part of the VM scale set"
  default     = "azureuser"
}

variable "admin_password" {
  description = "Default password for admin account"
  default = "Pass@word01"
}

# locals {
#   regions_with_availability_zones = ["centralus"] #["eastus","eastus2","eastus","westus"]
#   zones = tolist(["1", "2", "3"])
# }