terraform {
  required_version = ">=0.12"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}


provider "azurerm" {
  features {}
}


resource "azurerm_resource_group" "shirg" {
  name     = var.resource_group_name
  location = var.location
  tags     = var.tags
}


resource "azurerm_virtual_network" "shivnet" {
  name                = "shi-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = var.location
  resource_group_name = azurerm_resource_group.shirg.name
  tags                = var.tags
}


resource "azurerm_subnet" "shisubnet" {
  name                 = "shi-subnet"
  resource_group_name  = azurerm_resource_group.shirg.name
  virtual_network_name = azurerm_virtual_network.shivnet.name
  address_prefixes     = ["10.0.2.0/24"]
}


resource "azurerm_public_ip" "shipip" {
  name                = "shi-public-ip"
  location            = var.location
  resource_group_name = azurerm_resource_group.shirg.name
  allocation_method   = "Static"
  domain_name_label   = var.domain_name
  tags                = var.tags
}


resource "azurerm_lb" "shilb" {
  name                = "shi-lb"
  location            = var.location
  resource_group_name = azurerm_resource_group.shirg.name

  frontend_ip_configuration {
    name                 = "PublicIPAddress"
    public_ip_address_id = azurerm_public_ip.shipip.id
  }
  tags = var.tags
}


resource "azurerm_lb_backend_address_pool" "shibpepool" {
  loadbalancer_id     = azurerm_lb.shilb.id
  name                = "BackEndAddressPool"
}


resource "azurerm_lb_probe" "shiprobe" {
  resource_group_name = azurerm_resource_group.shirg.name
  loadbalancer_id     = azurerm_lb.shilb.id
  name                = "ssh-running-probe"
  port                = var.application_port
}


resource "azurerm_lb_rule" "shilbnatrule" {
  resource_group_name = azurerm_resource_group.shirg.name
  loadbalancer_id                = azurerm_lb.shilb.id
  name                           = "http"
  protocol                       = "Tcp"
  frontend_port                  = var.application_port
  backend_port                   = var.application_port
  frontend_ip_configuration_name = "PublicIPAddress"
  probe_id                       = azurerm_lb_probe.shiprobe.id
  backend_address_pool_ids = [azurerm_lb_backend_address_pool.shibpepool.id]
}


data "local_file" "webinit" {
  filename = "apachevirtualhost.sh"
}


resource "azurerm_linux_virtual_machine_scale_set" "shiss" {
  name                = "vmscaleset"
  location            = var.location
  resource_group_name = azurerm_resource_group.shirg.name
  sku                 = "Standard_DS1"
  instances           = 1
  admin_username      = var.admin_user
  admin_password = var.admin_password
  # zones = local.zones
  custom_data = base64encode(data.local_file.webinit.content)
  disable_password_authentication = false

#  admin_ssh_key {
#     username   = var.admin_user
#     public_key = file("~/.ssh/id_rsa.pub")
#   }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }

  data_disk {
    lun           = 0
    caching       = "ReadWrite"
    create_option = "Empty"
    disk_size_gb  = 2
    storage_account_type = "Standard_LRS"
  }

  network_interface {
    name    = "shi-vnet"
    primary = true

  ip_configuration {
    name      = "IPconfig"
    primary   = true
    subnet_id = azurerm_subnet.shisubnet.id
    load_balancer_backend_address_pool_ids = [azurerm_lb_backend_address_pool.shibpepool.id]
    }
  }

  # # This is to ensure SSH comes up before we run the local exec.
  # provisioner "remote-exec" {
  #   inline = [
  #     "sudo yum -y install httpd && sudo systemctl start httpd",
  #     "echo '<h1><center>My first website using terraform provisioner</center></h1>' > index.html",
  #     "echo '<h1><center>Jorge Gongora</center></h1>' >> index.html",
  #     "sudo mv index.html /var/www/html/"
  #   ]
  #   connection {
  #     type        = "ssh"
  #     host        = azurerm_public_ip.shipip.ip_address
  #     user        = var.admin_user
  #     # password = var.admin_password
  #     agent = true
  #     private_key = file("~/.ssh/id_rsa")
  # }
  # }
}

resource "azurerm_monitor_autoscale_setting" "vmss" {
  name                = "AutoscaleSetting"
  resource_group_name = azurerm_resource_group.shirg.name
  location            = azurerm_resource_group.shirg.location
  target_resource_id  = azurerm_linux_virtual_machine_scale_set.shiss.id

  profile {
    name = "defaultProfile"

    capacity {
      default = 1
      minimum = 1
      maximum = 2
    }

    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = azurerm_linux_virtual_machine_scale_set.shiss.id
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "GreaterThan"
        threshold          = 75
        metric_namespace   = "microsoft.compute/virtualmachinescalesets"
        dimensions {
          name     = "AppName"
          operator = "Equals"
          values   = ["App1"]
        }
      }

      scale_action {
        direction = "Increase"
        type      = "ChangeCount"
        value     = "1"
        cooldown  = "PT1M"
      }
    }

    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = azurerm_linux_virtual_machine_scale_set.shiss.id
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "LessThan"
        threshold          = 25
      }

      scale_action {
        direction = "Decrease"
        type      = "ChangeCount"
        value     = "1"
        cooldown  = "PT1M"
      }
    }
  }

  notification {
    email {
      send_to_subscription_administrator    = true
      send_to_subscription_co_administrator = true
      custom_emails                         = ["yustao@hotmail.com"]
    }
  }
}