#! /bin/bash

# update your base system with the latest available packages
apt-get update -y

# install the Apache web server 
apt-get install apache2 -y

# start the Apache service
systemctl start apache2

# You will need valid domain names to host multiple websites using name-based virtual hosting.
# create a document root directory for both websites
mkdir /var/www/html/www.test.com
mkdir /var/www/html/ww2.test.com

# create an index.html page for both websites
echo "<html>
<title>www.test.com</title>
<h1>hello test</h1>
<p>This is my first website hosting on one webserver for SHI Interview using Apache virtual hosting</p>
</html>" | sudo tee /var/www/html/www.test.com/index.html

echo "<html>
<title>ww2.test.com</title>
<h1>hello test2</h1>
<p>This is my second website hosting on one webserver for SHI Interview using Apache virtual hosting</p>
</html>" | sudo tee /var/www/html/www.test.com/index.html

# change the ownership of both websites directory to www-data
chown -R www-data:www-data /var/www/html/www.test.com
chown -R www-data:www-data /var/www/html/ww2.test.com

# create an Apache virtual host configuration file to serve both websites
echo "<VirtualHost *:80>
ServerAdmin admin@www.test.com
ServerName www.test.com
DocumentRoot /var/www/html/www.test.com
DirectoryIndex index.html
ErrorLog ${APACHE_LOG_DIR}/www.test.com_error.log
CustomLog ${APACHE_LOG_DIR}/www.test.com_access.log combined
</VirtualHost>" | sudo tee /etc/apache2/sites-available/www.test.com.conf

echo "<VirtualHost *:80>
ServerAdmin admin@ww2.test.com
ServerName ww2.test.com
DocumentRoot /var/www/html/ww2.test.com
DirectoryIndex index.html
ErrorLog ${APACHE_LOG_DIR}/ww2.test.com_error.log
CustomLog ${APACHE_LOG_DIR}/ww2.test.com_access.log combined
</VirtualHost>" | sudo tee /etc/apache2/sites-available/ww2.test.com.conf

# enable the virtual host configuration file
a2ensite www.test.com
a2ensite ww2.test.com

# restart the Apache webserver to apply the configuration changes
systemctl restart apache2

# Test Both Websites
#curl -H "Host:www.test.com" htt://<dns name label>.cloudapp.azure.com
#curl -H "Host:ww2.test.com" htt://<dns name label>.cloudapp.azure.com