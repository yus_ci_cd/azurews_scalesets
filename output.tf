output "shi_public_ip_fqdn" {
  value = azurerm_public_ip.shipip.fqdn
}

output "id" {
  value = azurerm_public_ip.shipip.id
}

output "ip_address" {
  value = azurerm_public_ip.shipip.ip_address
}
